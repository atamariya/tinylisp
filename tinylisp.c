/* tinylisp.c with NaN boxing by Robert A. van Engelen 2022 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>

#define I unsigned
#define L double
#define T(x) *(unsigned long long*)&x>>48
#define A (char*)cell
#define N 1024 * 3

I hp=0,sp=N,ATOM=0x7ff8,PRIM=0x7ff9,CONS=0x7ffa,CLOS=0x7ffb,MACR=0x7ffc,STRG=0x7ffd,NIL=0x7ffe;
L cell[N],nil,tru,env;
char buf[40],see = ' ';
FILE *in = NULL;
jmp_buf jb;

L read(), eval(L,L), parse();
void print(L);

L err(int i) { longjmp(jb,i); }
L box(I t,I i) { L x; *(unsigned long long*)&x = (unsigned long long)t<<48|i; return x; }
I ord(L x) { return *(unsigned long long*)&x; }
L num(L n) { return n; }
I equ(L x,L y) { return *(unsigned long long*)&x == *(unsigned long long*)&y; }
L box_str(const char *s, L t) {
 I i = 0; while (i < hp && strcmp(A+i,s)) i += strlen(A+i)+1;
 if (i == hp && (hp += strlen(strcpy(A+i,s))+1) > sp<<3) abort();
 return box(t,i);
}
L atom(const char *s)   { box_str(s, ATOM); }
L string(const char *s) { box_str(s, STRG); }
L cons(L x,L y) { cell[--sp] = x; cell[--sp] = y; if (hp > sp<<3) abort(); return box(CONS,sp); }
L car(L p) { return (T(p) == CONS || T(p) == CLOS || T(p) == MACR) ? cell[ord(p)+1] : err(1); }
L cdr(L p) { return (T(p) == CONS || T(p) == CLOS || T(p) == MACR) ? cell[ord(p)] : err(2); }
L pair(L v,L x,L e) { return cons(cons(v,x),e); }
L closure(L v,L x,L e) { return box(CLOS,ord(pair(v,x,equ(e,env) ? nil : e))); }
L macro(L v,L x) { return box(MACR,ord(cons(v,x))); }
L assoc(L v,L e) { while (T(e) == CONS && !equ(v,car(car(e)))) e = cdr(e); return T(e) == CONS ? cdr(car(e)) : nil; }
I not(L x) { return T(x) == NIL; }
I let(L x) { return T(x) != NIL && !not(cdr(x)); }
L evlis(L t,L e) { return T(t) == CONS ? cons(eval(car(t),e),evlis(cdr(t),e)) : T(t) == ATOM ? assoc(t,e) : nil; }

L f_eval(L t,L e) { return eval(car(evlis(t,e)),e); }
L f_quote(L t,L _) { return car(t); }
L f_cons(L t,L e) { return t = evlis(t,e),cons(car(t),car(cdr(t))); }
L f_car(L t,L e) { return car(car(evlis(t,e))); }
L f_cdr(L t,L e) { return cdr(car(evlis(t,e))); }
L f_add(L t,L e) { L n = car(t = evlis(t,e)); while (!not(t = cdr(t))) n += car(t); return num(n); }
L f_sub(L t,L e) { L n = car(t = evlis(t,e)); while (!not(t = cdr(t))) n -= car(t); return num(n); }
L f_mul(L t,L e) { L n = car(t = evlis(t,e)); while (!not(t = cdr(t))) n *= car(t); return num(n); }
L f_div(L t,L e) { L n = car(t = evlis(t,e)); while (!not(t = cdr(t))) n /= car(t); return num(n); }
L f_int(L t,L e) { L n = car(evlis(t,e)); return n<1e16 && n>-1e16 ? (long long)n : n; }
L f_lt(L t,L e) { return t = evlis(t,e),car(t) - car(cdr(t)) < 0 ? tru : nil; }
L f_eq(L t,L e) { return t = evlis(t,e),equ(car(t),car(cdr(t))) ? tru : nil; }
L f_not(L t,L e) { return not(car(evlis(t,e))) ? tru : nil; }
L f_or(L t,L e) { L x = nil; while (T(t) != NIL && not(x = eval(car(t),e))) t = cdr(t); return x; }
L f_and(L t,L e) { L x = nil; while (T(t) != NIL && !not(x = eval(car(t),e))) t = cdr(t); return x; }
L f_cond(L t,L e) { while (T(t) != NIL && not(eval(car(car(t)),e))) t = cdr(t); return eval(car(cdr(car(t))),e); }
L f_if(L t,L e) { return eval(car(cdr(not(eval(car(t),e)) ? cdr(t) : t)),e); }
L f_leta(L t,L e) { for (;let(t); t = cdr(t)) e = pair(car(car(t)),eval(car(cdr(car(t))),e),e); return eval(car(t),e); }
L f_lambda(L t,L e) { return closure(car(t),car(cdr(t)),e); }
L f_define(L t,L e) { env = pair(car(t),eval(car(cdr(t)),e),env); return car(t); }
L f_macro(L t,L e) { return macro(car(t),car(cdr(t))); }
L f_catch(L t,L e) {
  L x; int i;
  jmp_buf savedjb;
  memcpy(savedjb,jb,sizeof(jb));
  i = setjmp(jb);
  x = i ? cons(atom("ERR"),i) : eval(car(t),e);
  memcpy(jb,savedjb,sizeof(jb));
  return x;
}
L f_throw(L t,L e) { longjmp(jb,(int)num(car(t))); }
L f_setq(L t,L e) {
  L v = car(t),x = eval(car(cdr(t)),e);
  while (T(e) == CONS && !equ(v, car(car(e)))) e = cdr(e);
  return T(e) == CONS ? cell[ord(car(e))] = x : (env = pair(v,x,env),x);
}
L f_print(L t,L e) { print(car(t)); print(e); return nil; }
L f_load(L t,L e) {
  L x = car(evlis(t,e)); char s[256];
  sprintf(s,"%s",A+ord(x)); printf("%s",A+ord(x));
  if (T(x) != STRG) err(4);
  { FILE *tmp = in; in = fopen(s,"r"); x = eval(read(),e); in = tmp; };
  return x;
}

struct { const char *s; L (*f)(L,L); } prim[] = {
  {"eval",f_eval}, {"quote",f_quote}, {"cons",f_cons},{"car", f_car}, {"cdr", f_cdr},
  {"+",   f_add},  {"-",  f_sub},     {"*",   f_mul}, {"/",   f_div}, {"int", f_int},
  {"<",   f_lt},   {"eq", f_eq},      {"or",  f_or},  {"and", f_and}, {"not", f_not},
  {"cond",f_cond}, {"if", f_if},      {"let*",f_leta},{"lambda",f_lambda},
  {"define",f_define}, {"macro",f_macro}, {"catch", f_catch}, {"throw", f_throw},
  {"setq",f_setq}, {"p", f_print},    {"load",f_load},{0}};
L bind(L v,L t,L e) { return T(v) == NIL ? e : T(v) == CONS ? bind(cdr(v),cdr(t),pair(car(v),car(t),e)) : pair(v,t,e); }
L expand(L f,L t,L e) { return eval(eval(cdr(f),bind(car(f),t,env)),e); }
L reduce(L f,L t,L e) { return eval(cdr(car(f)),bind(car(car(f)),evlis(t,e),not(cdr(f)) ? env : cdr(f))); }
L apply(L f,L t,L e) { return T(f) == PRIM ? prim[ord(f)].f(t,e) : T(f) == CLOS ? reduce(f,t,e) : T(f) == MACR ? expand(f,t,e) : err(3); }
L step(L x,L e) { return T(x) == ATOM ? assoc(x,e) : T(x) == CONS ? apply(eval(car(x),e),cdr(x),e) : x; }
L eval(L x,L e) {
  L y = step(x,e);
  if (1) {
	printf("\n%u: ",sp); print(x); printf(" => "); print(y);
	while (0 && getchar() >= ' ') continue;
  }
  return y;
}

void look() { int c = in ? getc(in) : getchar(); see = c; if (c == EOF) if (in) {fclose(in); in = NULL;} else exit(0); }
I seeing(char c) { return c == ' ' ? see > 0 && see <= c : see == c; }
char get() { char c = see; look(); return c; }
char scan() {
 int i = 0;
 while (seeing(' ') || seeing(';')) if (get() == ';') while (!seeing('\n')) look();
 if (seeing('(') || seeing(')') || seeing('\'')) buf[i++] = get();
 else if (seeing('"')) { do buf[i++] = get(); while (!seeing('"')); look(); }
 else do buf[i++] = get(); while (i < 39 && !seeing('(') && !seeing(')') && !seeing(' '));
 return buf[i] = 0,*buf;
}
L read() { return scan(),parse(); }
L list() { L x; return scan() == ')' ? nil : !strcmp(buf, ".") ? (x = read(),scan(),x) : (x = parse(),cons(x,list())); }
L quote() { return cons(atom("quote"),cons(read(),nil)); }
L atomic() { L n; int i; return sscanf(buf,"%lg%n",&n,&i) > 0 && !buf[i] ? n : atom(buf); }
L parse() { return *buf == '(' ? list() : *buf == '\'' ? quote() : *buf == '"' ? string(buf+1) : atomic(); }
void printlist(L t) {
 for (putchar('('); ; putchar(' ')) {
  print(car(t));
  if (not(t = cdr(t))) break;
  if (T(t) != CONS) { printf(" . "); print(t); break; }
 }
 putchar(')');
}
void print(L x) {
 if (T(x) == NIL) printf("()");
 else if (T(x) == ATOM) printf("%s",A+ord(x));
 else if (T(x) == STRG) printf("%s",A+ord(x));
 else if (T(x) == PRIM) printf("<%s>",prim[ord(x)].s);
 else if (T(x) == CONS) printlist(x);
 else if (T(x) == CLOS) printf("{%u}",ord(x));
 else printf("%.10lg",x);
}
void gc() {
  sp = ord(env);
  for (int i = 0; i < N; ++i) if (T(cell[i]) == ATOM && ord(cell[i]) > hp) hp = ord(cell[i]);
  hp += strlen(A+hp)+1;
}
int main(int argc,char **argv) {
 int i;
 printf("tinylisp");
 nil = box(NIL,0); atom("ERR"); tru = atom("t"); env = pair(tru,tru,nil);
 for (i = 0; prim[i].s; ++i) env = pair(atom(prim[i].s),box(PRIM,i),env);
 in = fopen((argc > 1 ? argv[1] : "init.lisp"),"r");
 if ((i = setjmp(jb)) != 0) printf("ERR %d",i);
 while (1) { gc(); printf("\n%u>",sp-hp/8); print(eval(read(),env)); }
}
